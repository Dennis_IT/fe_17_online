/*

Функция определяет собственную (локальную) область видимости, куда входят входные параметры, а также те переменные, которые объявляются непосредственно в теле самой функции.

Значение, передаваемое в качестве параметра функции, также называется аргументом. Другими словами: Параметр – это переменная, указанная в круглых скобках в объявлении функции. Аргумент – это значение, которое передаётся функции при её вызове.

Оператор return завершает выполнение функции и возвращает управление вызывающей функции.

 */



let numOne;
let numTwo;

do {
    numOne = +prompt("Enter number one :", numOne);
    numTwo = +prompt("Enter number two :", numTwo)
} while (!numOne || !numTwo)


let operation = prompt("Enter operation :");


function count(num1, num2, mark) {
    switch (mark) {
        case "*":
            return num1 * num2;
        case "+":
            return num1 + num2;
        case "-":
            return num1 - num2;
        case "/":
            if (num2 !== 0) {
                return num1 / num2;
            } else {
                return " is not divisible by zero";
            }
    }
}

console.log(count(numOne, numTwo, operation))