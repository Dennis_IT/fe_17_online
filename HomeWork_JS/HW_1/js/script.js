//В JavaScript есть три вида объявления переменных:
// var
// let
// const
//
//Функция confirm отображает модальное окно с текстом вопроса, и двумя кнопками: OK и Отмена.
//Функция prompt отобразит модальное окно с текстом, полем для ввода текста и кнопками OK/Отмена.
//
//
//   Неявным приведением типов обычно преобразование происходит, когда в выражениях используют значения различных типов.
//   Для явного преобразования в примитивные типы данных в JavaScript используются следующие функции: Boolean(), Number(), String()
//   let trans = [];
//     trans[0] = 1 + "true";    //'1true'
//     trans[1] = 1 + true;       //2
//     trans[2] = 1 + undefined; //NaN
//     trans[3] = 1 + null;           //1
//     trans[4] = "2" > 10;        //false      2>10?
//     trans[5] = "2" > '10';       //true     '2'. charCodeAt()>'10'. charCodeAt()
//     trans [6] = "abc"> "b"; // ложь Сравнить слева направо 'b'> 'a'
//     trans[7] = "abc" > "aad"; //true
//     trans[8] = NaN == NaN;           //false
//     trans[9] = undefined == null;    //true
//     trans[10] = undefined == undefined; //true
//     trans[11] = null == null;            //true



let admin;
let name;

name = "denis";
admin = name;
console.log(admin);

let days = +prompt("Введите число от 1 до 10")
console.log(days*86400 + " second");

console.log(prompt("Enter your name", "NAME..."));



